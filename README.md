A Brief Introduction to the Cardiotocography Data Set and the Prediction Task:

Cardiotocography (CTG) in medical terms is a technical means of recording the fetal 
heartbeat and uterine contractions during pregnancy, typically in the third trimester. 
This gives information on the uterine contractions, Baseline fetal Heart Rate and other 
measures necessary for the Obstetrician to gauge the well-being of the fetus and suggest 
possible time of delivery. An improper CTG measure could suggest possibility of having to 
deliver through Caesarean section. Hence to ensure accurate prediction of fetal well-being
and prepare the mother for delivery, 
the data contained in a CTG needs to be appropriately interpreted.

The dataset is collected from UCI Machine Learning repository, present here:
http://odds.cs.stonybrook.edu/cardiotocogrpahy-dataset/

Data Dictionary:

Attribute Information:

LB – FHR baseline (beats per minute)

AC – No. of accelerations per second

FM – No. of fetal movements per second

UC – No.  of uterine contractions per second

DL – No. of light decelerations per second

DS – No.  of severe decelerations per second

DP – No.  of prolongued decelerations per second

ASTV – Percentage of time with abnormal short term variability

MSTV – Mean/Average value of short term variability

ALTV – Percentage of time with abnormal long term variability

MLTV – Mean/Average value of long term variability

Width – width of FHR histogram

Min – minimum of FHR histogram

Max – Maximum of FHR histogram

Nmax – No.  of histogram peaks

Nzeros – No.  of histogram zeros

Mode – histogram mode

Mean – histogram mean

Median – histogram median

Variance – histogram variance

Tendency – histogram tendency

CLASS – FHR pattern class code having codes from 1 to 10)

NSP – Fetal state class code, where N=normal; S=suspect; P=pathologic

Here, we are considering NSP=1 as Inliers and NSP=3 as Outliers

Total number of Inliers=1658

Total Number of Outliers=178